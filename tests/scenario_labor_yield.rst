====================================
Labor Yield Scenario
====================================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> import datetime


Install stock_unit_load_labor_yield::

    >>> config = activate_modules('labor_yield')


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get uom and uom category::

    >>> UomCategory = Model.get('product.uom.category')
    >>> Uom = Model.get('product.uom')
    >>> uom_cat_unit, = UomCategory.find([('name', '=', 'Units')])
    >>> uom_unit, = Uom.find([('name', '=', 'Unit')])


Create work::

    >>> Work = Model.get('timesheet.work')
    >>> work = Work(name='Allocation Procedure', yield_available=True)
    >>> work.manual_yield_record = True
    >>> work.uom_category == uom_cat_unit
    True
    >>> work.save()


Check enter wizard::

    >>> enter = Wizard('labor.yield.enter')
    >>> enter.form.work = work
    >>> enter.execute('enter')


Check allocate wizard::

    >>> Procedure = Model.get('labor.yield.allocation.procedure')
    >>> procedure = Procedure(name='Allocation Procedure')
    >>> procedure.save()
    >>> allocate = Wizard('labor.yield.allocate')
    >>> allocate.execute('get_params')
    >>> allocate.execute('summary')
    >>> allocate.execute('do_')


Create employee::

    >>> Party = Model.get('party.party')
    >>> Employee = Model.get('company.employee')
    >>> party = Party(name='Employee1')
    >>> party.save()
    >>> employee1 = Employee(party=party, company=company)
    >>> employee1.save()


Create manual allocation::

    >>> config._context['work'] = work.id
    >>> Alloc = Model.get('labor.yield.allocation')
    >>> alloc = Alloc()
    >>> alloc.work == work
    True
    >>> alloc.quantity_uom == uom_unit
    True
    >>> alloc.quantity = 100
    >>> alloc.employee = employee1
    >>> alloc.save()