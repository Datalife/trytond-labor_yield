datalife_labor_yield
====================

The labor_yield module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-labor_yield/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-labor_yield)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
