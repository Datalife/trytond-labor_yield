# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval, Bool, Not, PYSONEncoder, Date, And
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateView, StateAction, Button
from trytond.wizard import StateTransition
from .labor_yield import YieldAllocationMixin
from itertools import groupby


class YieldEnterGetParams(ModelView):
    """Labor Yield Enter Get Params"""
    __name__ = 'labor.yield.enter.get_params'
    work = fields.Many2One('timesheet.work', 'Work', required=True,
        domain=[
            ('yield_available', '=', True),
            ('manual_yield_record', '=', True)])
    date = fields.Date('Date', required=True)

    @staticmethod
    def default_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()


class YieldEnter(Wizard):
    """Labor Yield Enter"""
    __name__ = 'labor.yield.enter'
    start = StateTransition()
    get_params = StateView(
        'labor.yield.enter.get_params',
        'labor_yield.enter_get_params_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Enter', 'enter', 'tryton-ok', default=True)])
    enter = StateAction('labor_yield.act_yield_allocation')

    def transition_start(self):
        return 'get_params'

    def do_enter(self, action):
        pool = Pool()
        Lang = pool.get('ir.lang')
        pyson_domain = [
            ('procedure', '=', None),
            ('offset_', '=', None),
            ('source', '=', None),
            ('work', '=', self.get_params.work.id)]
        pyson_context = {'work': self.get_params.work.id}
        action['name'] += " - " + self.get_params.work.rec_name
        date = self.get_params.date
        date = Date(date.year, date.month, date.day)
        if self.get_params.work.yield_record_granularity == 'employee':
            date = self.get_params.date
            date = Date(date.year, date.month, date.day)
            pyson_domain.append(('date', '=', date))
            pyson_context['date'] = date
        else:
            pyson_domain.append(('date', '>=', date))
        action['pyson_domain'] = PYSONEncoder().encode(pyson_domain)
        action['pyson_context'] = PYSONEncoder().encode(pyson_context)
        for code in [Transaction().language, 'en_US']:
            langs = Lang.search([
                ('code', '=', code)])
            if langs:
                break
        lang, = langs
        date = lang.strftime(self.get_params.date)
        action['name'] += " - " + self.get_params.work.rec_name
        action['name'] += " @ %s" % date
        return action, {}

    def transition_enter(self):
        return 'end'


class YieldAllocateProcedure(ModelView):
    """Labor Yield Allocate Procedure Select"""
    __name__ = 'labor.yield.allocate.procedure'

    procedure = fields.Many2One('labor.yield.allocation.procedure',
        'Procedure', required=True)


class YieldAllocateGetParams(ModelView):
    """Labor Yield Allocate Get Params"""
    __name__ = 'labor.yield.allocate.get_params'

    date = fields.Date('Date', required=True)


class YieldAllocateSummaryDet(YieldAllocationMixin, ModelView):
    """Labor Yield Allocate Summary Detail"""
    __name__ = 'labor.yield.allocate.summary.detail'

    allocated = fields.Float(
        'Allocated', digits=(16, Eval('quantity_digits', 2)),
        readonly=True,
        depends=['quantity_digits'])

    manual = fields.Float(
        'Manual', digits=(16, Eval('quantity_digits', 2)),
        readonly=True,
        depends=['quantity_digits'])

    offset_1 = fields.Float(
        'Offset 1', digits=(16, Eval('quantity_digits', 2)),
        states={
            'readonly': Not(And(
                Bool(Eval('procedure')),
                Bool(Eval('employee'))))},
        depends=['quantity_digits', 'employee', 'procedure'])

    offset_2 = fields.Float(
        'Offset 2', digits=(16, Eval('quantity_digits', 2)),
        states={
            'readonly': Not(And(
                Bool(Eval('procedure')),
                Bool(Eval('employee'))))},
        depends=['quantity_digits', 'employee', 'procedure'])

    offset_3 = fields.Float(
        'Offset 3', digits=(16, Eval('quantity_digits', 2)),
        states={
            'readonly': Not(And(
                Bool(Eval('procedure')),
                Bool(Eval('employee'))))},
        depends=['quantity_digits', 'employee', 'procedure'])

    @classmethod
    def __setup__(cls):
        super(YieldAllocateSummaryDet, cls).__setup__()
        cls.employee.required = False
        cls.employee.readonly = True
        cls.quantity.readonly = True

    @fields.depends('offset_1', 'offset_2', 'offset_3', 'quantity',
        'employee', 'manual')
    def on_change_with_allocated(self):
        if self.employee:
            return (
                (self.quantity or 0) + (self.offset_1 or 0)
                + (self.offset_2 or 0) + (self.offset_3 or 0)
                + (self.manual or 0))
        return 0


class YieldAllocateSummary(ModelView):
    """Labor Yield Allocate Summary"""
    __name__ = 'labor.yield.allocate.summary'

    date = fields.Date('Date', readonly=True)
    details = fields.One2Many('labor.yield.allocate.summary.detail', None,
                              'Yield Details')


class YieldAllocate(Wizard):
    """Labor Yield Allocate"""
    __name__ = 'labor.yield.allocate'

    start = StateTransition()
    procedure = StateView(
        'labor.yield.allocate.procedure',
        'labor_yield.allocate_procedure_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'get_params', 'tryton-ok', default=True)])
    get_params = StateView(
        'labor.yield.allocate.get_params',
        'labor_yield.allocate_get_params_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Summary', 'summary', 'tryton-launch', default=True)])
    summary = StateView(
        'labor.yield.allocate.summary',
        'labor_yield.allocate_summary_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Save', 'offset_', 'tryton-launch', default=True),
         Button('Allocate', 'do_', 'tryton-launch')])
    offset_ = StateTransition()
    do_ = StateTransition()

    def transition_start(self):
        return 'procedure'

    def default_procedure(self, fields):
        pool = Pool()
        YieldAllocateProcedure = pool.get('labor.yield.allocation.procedure')
        res = YieldAllocateProcedure.search([])
        return {'procedure': res[0].id if res else None}

    def default_get_params(self, fields):
        Date_ = Pool().get('ir.date')
        return {'date': Date_.today()}

    def _get_allocations(self, procedure_id):
        return self.procedure.procedure.get_allocations(
            self.get_params.date)

    def _get_unallocated(self, procedure_id):
        return self.procedure.procedure.get_unallocated(
            self.get_params.date)

    def _aggr_allocations(self, allocations, procedure_id):
        details = []
        allocations.sort(key=lambda a: a.get('employee'))
        for employee, eallocs_ in groupby(allocations,
                                          lambda x: x.get('employee')):
            eallocs = list(eallocs_)
            quantity = sum(ea.get('quantity') for ea in eallocs)
            details.append({
                'procedure': eallocs[0].get('procedure'),
                'work': eallocs[0].get('work'),
                'employee': employee,
                'quantity': quantity,
                'allocated': quantity})
        return details

    def _aggr_unallocated(self, unallocated, procedure_id):
        return [{
            'procedure': unallocated[0].get('procedure'),
            'work': unallocated[0].get('work'),
            'employee': None,
            'quantity': sum(ua.get('quantity') for ua in unallocated),
            'allocated': 0}]

    def _summ_detail_sort(self, details, procedure_id):
        pool = Pool()
        Employee = pool.get('company.employee')
        employees = {e.id: e.party.name for e in
                     Employee.browse([d['employee'] for d in details])}
        details.sort(key=lambda x: employees.get(
            x.get('employee', None), None))
        return details

    def _get_offset_(self, details):
        pool = Pool()
        procedure = self.procedure.procedure
        Allocation = pool.get('labor.yield.allocation')
        date_ = self.get_params.date
        res = Allocation.search([
            ('procedure', '=', procedure.id),
            ('date', '=', date_),
            ('offset_', 'in', [1, 2, 3])])
        offsets = {(r.employee.id, r.offset_): r.quantity for r in res}
        for d in details:
            if d.get('employee', None):
                for i in range(1, 4):
                    d['offset_%s' % i] = offsets.get((d['employee'], i), 0)
        return details

    def _get_manuals(self, details):
        pool = Pool()
        procedure = self.procedure.procedure
        Allocation = pool.get('labor.yield.allocation')
        date_ = self.get_params.date
        res = Allocation.search([
            ('work', '=', procedure.work.id),
            ('procedure', '=', None),
            ('date', '=', date_)])
        computed = {d['employee']: d for d in details
                    if d.get('employee', None)}
        for r in res:
            d = computed.get(r.employee.id, {
                'work': procedure.work.id,
                'employee': r.employee.id})
            d['manual'] = r.quantity
            if not d.get('procedure', None):
                details.append(d)
        return details

    def default_summary(self, fields):
        proc_id = self.procedure.procedure.id
        result = {'date': self.get_params.date}
        details = []
        yield_allocs = self._get_allocations(proc_id)
        if yield_allocs:
            details = self._aggr_allocations(yield_allocs, proc_id)
        unallocated = self._get_unallocated(proc_id)
        if unallocated:
            details.extend(self._aggr_unallocated(unallocated, proc_id))
        if details:
            details = self._get_offset_(details)
            details = self._get_manuals(details)
            result['details'] = self._summ_detail_sort(details, proc_id)
        return result

    def _delete_yield_allocations(self, offset_=False):
        pool = Pool()
        procedure = self.procedure.procedure
        Allocation = pool.get('labor.yield.allocation')
        date_ = self.get_params.date
        _domain = [
            ('date', '=', date_),
            ('procedure', '=', procedure)]
        if offset_:
            _domain.append(('offset_', '>', 0))
        to_delete = Allocation.search(_domain)
        Allocation.delete(to_delete)

    def _get_offset_allocations(self):
        procedure = self.procedure.procedure
        date_ = self.get_params.date
        offset_allocs = []
        for sd in self.summary.details:
            for i in range(1, 4):
                offset_qty = getattr(sd, 'offset_%s' % i, 0) or 0
                if offset_qty != 0:
                    offset_allocs.append(
                        {'work': procedure.work.id,
                         'procedure': procedure.id,
                         'offset_': i,
                         'date': date_,
                         'employee': sd.employee.id,
                         'quantity': offset_qty,
                         'quantity_uom': procedure.work.default_uom.id})
        return offset_allocs

    def transition_offset_(self):
        pool = Pool()
        Allocation = pool.get('labor.yield.allocation')
        self._delete_yield_allocations(offset_=True)
        offset_allocs = self._get_offset_allocations()
        if offset_allocs:
            with Transaction().set_context(active_test=False):
                Allocation.create(offset_allocs)
        return 'end'

    def _write_yield_allocations(self):
        pool = Pool()
        procedure = self.procedure.procedure
        Allocation = pool.get('labor.yield.allocation')
        yield_allocs = self._get_allocations(procedure.id)
        yield_allocs.extend(self._get_offset_allocations())
        if yield_allocs:
            with Transaction().set_context(active_test=False):
                Allocation.create(yield_allocs)

    def transition_do_(self):
        self._delete_yield_allocations()
        self._write_yield_allocations()
        return 'end'


class YieldAllocateSummaryDet3(metaclass=PoolMeta):
    __name__ = 'labor.yield.allocate.summary.detail'

    employee_code = fields.Function(fields.Char('Employee Code'),
        'on_change_with_employee_code')

    @fields.depends('employee')
    def on_change_with_employee_code(self, name=None):
        return self.employee.code if self.employee else None
